package com.nicopaez;

import org.junit.Assert;
import org.junit.Test;

public class StringTest {
    @Test
    public void convertsIntToString() {
        String twoToString = String.valueOf(2);
        Assert.assertEquals("2",twoToString);
    }

    @Test
    public void convertsBooleanToString(){
        String trueToString = String.valueOf(Boolean.TRUE);
        Assert.assertEquals("true", trueToString);
    }

    @Test
    public void joinsStringsWithUnderscore(){
        String joinedString = String.join("_","ABC","DEF","GHI");
        Assert.assertEquals("ABC_DEF_GHI", joinedString);
    }

    @Test
    public void convertsAStringToLowerCase(){
        String stringUpper = new String("ABcDeFGHI");
        String stringToLowercase = stringUpper.toLowerCase();
        Assert.assertEquals("abcdefghi", stringToLowercase);
    }

    @Test
    public void stringIsFormattedWithCorrectParametersAndTypes(){
        String formattedString = String.format("My name is %s and I'm %d.", "Luciana", 24);
        Assert.assertEquals("My name is Luciana and I'm 24.", formattedString);
    }

}
