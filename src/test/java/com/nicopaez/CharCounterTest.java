package com.nicopaez;
import org.junit.Test;
import java.util.HashMap;
public class CharCounterTest {

    @Test
    public void whenInputIsEmptyResultIsEmpty() {
        CharCounter counter = new CharCounter();
        HashMap<Character, Integer> result =  counter.countAll("");
        assert(result.isEmpty());
    }

    @Test
    public void itCountsLowercaseCharCorrectly() {
        CharCounter counter = new CharCounter();
        HashMap<Character, Integer> result =  counter.countAll("calabaza");
        Integer countA = result.get('a');
        assert(countA == 4);
    }

    @Test
    public void upperAndLowercaseAreEquallyManaged() {
        CharCounter counter = new CharCounter();
        HashMap<Character, Integer> result =  counter.countAll("aAaAaAA");
        Integer countA = result.get('a');
        assert(countA == 7);
    }

    @Test
    public void spaceShouldBeManagedAsAChar() {
        CharCounter counter = new CharCounter();
        HashMap<Character, Integer> result =  counter.countAll("    ");
        Integer count = result.get(' ');
        assert(count == 4);
    }

    @Test
    public void hyphenCountShouldAlwaysBeZero() {
        CharCounter counter = new CharCounter();
        HashMap<Character, Integer> result =  counter.countAll("A-B-C-D");
        Integer count = result.get('-');
        assert(count == 0);
    }

    @Test
    public void hyphenMustBeCountedAsUnderscores() {
        CharCounter counter = new CharCounter();
        HashMap<Character, Integer> result =  counter.countAll("---___");
        Integer count = result.get('_');
        assert(count == 6);
    }
}
